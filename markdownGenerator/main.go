package main

import (
	"fmt"
	"github.com/russross/blackfriday"
	"net/http"
	"os"
)

func main() {
	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}
	fmt.Println("running on Port: " + port)

	http.HandleFunc("/markdown", GenerateMarkdown)
	http.Handle("/", http.FileServer(http.Dir("public")))
	http.ListenAndServe(":"+port, nil)
}

func GenerateMarkdown(rw http.ResponseWriter, r *http.Request) {

	markdown := blackfriday.Markdown([]byte(r.FormValue("markupInputField")), blackfriday.HtmlRenderer(blackfriday.HTML_COMPLETE_PAGE, "Markdown", "css/bootstrap.min.css"), 0)
	rw.Write(markdown)
}
